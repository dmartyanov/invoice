name := "coltrx"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  "net.databinder.dispatch"   %% "dispatch-core"      % "0.11.0",
    "org.scalaj"                %% "scalaj-http"        % "0.3.12",
    //"org.json4s"                %% "json4s-native"      % "3.2.10",
    "org.json4s"                %% "json4s-jackson"     % "3.2.10",
    "org.scalatest"             %% "scalatest"          % "2.0"         % "test",
    "org.jsoup"                 % "jsoup"               % "1.7.3",
    "org.liquibase" % "liquibase-core" % "2.0.5",
    "postgresql" % "postgresql" % "9.1-901.jdbc4",
    "org.squeryl" %% "squeryl" % "0.9.5-6"
)

   scalacOptions ++= Seq(
       "-encoding",
       "UTF-8",
       "-deprecation",
       "-unchecked",
       "-feature",
       "-language:postfixOps",
       "-language:implicitConversions"
     )

play.Project.playScalaSettings
