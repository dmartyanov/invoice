package transportModel

/**
 * Created with IntelliJ IDEA.
 * User: hellraiser
 * Date: 7/27/14
 * Time: 6:36 AM
 * To change this template use File | Settings | File Templates.
 */
case class TDiff (
                  person: Option[TPerson],
                  diff: Double
                   )
