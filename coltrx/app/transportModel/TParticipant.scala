package transportModel


/**
 * Created with IntelliJ IDEA.
 * User: hellraiser
 * Date: 7/26/14
 * Time: 5:37 PM
 * To change this template use File | Settings | File Templates.
 */

case class TParticipant(
                         person: Option[TPerson],
                         amount: Option[Double],
                         payed: Option[Double] = None
                         )
