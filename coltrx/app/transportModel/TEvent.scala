package transportModel

import java.sql.Timestamp


/**
 * Created with IntelliJ IDEA.
 * User: hellraiser
 * Date: 7/26/14
 * Time: 5:36 PM
 * To change this template use File | Settings | File Templates.
 */
case class TEvent(
                   author: Option[TPerson],
                   title: String,
                   description: Option[String] = None,
                   parts: List[TParticipant] = List(),
                   etype: Option[Int] = Some(0),
                   amount: Double = 0,
                   payed: Option[Double] = None,
                   id: Option[String] = None,
                   date: Option[Timestamp] = None,
                   location: Option[String] =None
                   )
