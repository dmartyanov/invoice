package plugins

import scala.collection.JavaConversions._
import liquibase.changelog.ChangeSet
import liquibase.snapshot.DatabaseSnapshotGeneratorFactory
import liquibase.logging.LogFactory
import liquibase.resource.{CompositeResourceAccessor, ClassLoaderResourceAccessor, FileSystemResourceAccessor}
import liquibase.Liquibase
import liquibase.database.jvm.JdbcConnection
import liquibase.snapshot.jvm.PostgresDatabaseSnapshotGenerator
import liquibase.database.Database
import play.api._
import java.sql.Connection
import play.api.db.DB

/**
 * Created with IntelliJ IDEA.
 * User: hellraiser
 * Date: 7/26/14
 * Time: 2:20 PM
 * To change this template use File | Settings | File Templates.
 */
class LiquibasePlugin (app: Application) extends Plugin {
  val TestContext = "test"
  val DeveloperContext = "dev"
  val ProductionContext = "prod"

  private def getScriptDescriptions(changeSets: Seq[ChangeSet]) = {
    changeSets.zipWithIndex.map {
      case (cl, num) =>
        "" + num + ". " + cl.getId +
          Option(cl.getDescription).map(" (" + _ + ")").getOrElse("") +
          " by " + cl.getAuthor
    }.mkString("\n")
  }

  override def onStart() {
    DatabaseSnapshotGeneratorFactory.getInstance.register(new LogPostgresLiquibaseGenerator)

    val dbName = "default"

    LogFactory.getLogger.info("Running migrations")
    DB.withConnection(dbName)(connection => {
      val liqui =  try {getLiquibase(connection)} catch { case e: Exception => throw new PlayException("Liquibase module doesn't exist","", e)}
      app.mode match {
        case Mode.Test => liqui.update(TestContext)
        case Mode.Dev if app.configuration.getBoolean("applyLiquibase." + dbName).filter(_ == true).isDefined => liqui.update(DeveloperContext)
        case Mode.Prod if app.configuration.getBoolean("applyLiquibase." + dbName).filter(_ == true).isDefined => liqui.update(ProductionContext)
        case Mode.Prod => {
          Logger("play").warn("Your production database [" + dbName + "] needs Liquibase updates! \n\n" + getScriptDescriptions(liqui.listUnrunChangeSets(ProductionContext)))
          Logger("play").warn("Run with -DapplyLiquibase." + dbName + "=true if you want to run them automatically (be careful)")

          throw new PlayException("Liquibase script should be applyed, set applyLiquibase."+dbName+"=true in application.conf", getScriptDescriptions(liqui.listUnrunChangeSets(ProductionContext)))
        }
        case _ => throw new PlayException("Liquibase script should be applyed, set applyLiquibase."+dbName+"=true in application.conf", getScriptDescriptions(liqui.listUnrunChangeSets(ProductionContext)))
      }
    })(app)
  }

  def getLiquibase(connection: Connection) = {
    val fileAccessor = new FileSystemResourceAccessor(app.path.getAbsolutePath)
    val classLoaderAccessor = new ClassLoaderResourceAccessor(app.classloader)
    val fileOpener = new CompositeResourceAccessor(fileAccessor, classLoaderAccessor)
    new Liquibase("migrations.xml", fileOpener, new JdbcConnection(connection))
  }

  override lazy val enabled = {
    app.configuration.getConfig("db").isDefined && {
      !app.configuration.getString("liquibaseplugin").filter(_ == "disabled").isDefined
    }
  }
}

class LogPostgresLiquibaseGenerator extends PostgresDatabaseSnapshotGenerator
{
  override def convertTableNameToDatabaseTableName(tableName: String): String =  {
    tableName;
  }

  override def getPriority(database: Database): Int = {
    super.getPriority(database) + 1;
  }
}
