package dbmodel

import org.squeryl.dsl.CompositeKey2
import org.squeryl.KeyedEntity

/**
 * Created with IntelliJ IDEA.
 * User: hellraiser
 * Date: 7/26/14
 * Time: 3:14 PM
 * To change this template use File | Settings | File Templates.
 */
case class ParticipantSet(
                           eventid: String,
                           personid: String,
                           loanamount: Double,
                           payed: Option[Double]
                           ) extends KeyedEntity[CompositeKey2[String, String]] {
  def id: CompositeKey2[String, String] = CompositeKey2(eventid, personid)
}
