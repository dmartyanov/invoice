package dbmodel

import org.squeryl.KeyedEntity
import java.util.Date
import transportModel.TPerson
import java.sql.Timestamp

/**
 * Created with IntelliJ IDEA.
 * User: hellraiser
 * Date: 7/26/14
 * Time: 2:56 PM
 * To change this template use File | Settings | File Templates.
 */
case class InvoiceEvent(
                         id: String,
                         title: String,
                         place: String,
                         description: Option[String],
                         amount: Double,
                         etype: Int,
                         author: String,
                         date: Option[Timestamp],
                          location: Option[String]
                         ) extends KeyedEntity[String]

