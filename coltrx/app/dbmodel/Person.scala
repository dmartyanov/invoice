package dbmodel

import org.squeryl.KeyedEntity

/**
 * Created with IntelliJ IDEA.
 * User: hellraiser
 * Date: 7/26/14
 * Time: 2:58 PM
 * To change this template use File | Settings | File Templates.
 */
case class Person(
                   id: String,
                   phone: String,
                   fullname: Option[String]
                   ) extends KeyedEntity[String]
