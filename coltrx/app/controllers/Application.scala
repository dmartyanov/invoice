package controllers

import play.api.mvc._
import utils.JsonHelper
import apihandler.ApiHandler
import transportModel.{TParticipant, TPerson, TEvent}

object Application extends Controller with JsonHelper {

  def index = Action {
    Ok(views.html.index("Invoice System"))
  }

  def test = Action {

    println(produceJS(TEvent(
      Some(TPerson("9252229788", Option("Holden Caulfield"))),
      title = "Shop",
      description = Option("description"),
      parts = List(
        TParticipant(
          person = Option(TPerson("00000001", Option("Tarasenko"))),
          amount = Option(100)
        ),
        TParticipant(
          person = Option(TPerson("00005601", Option("Alexander Serij"))),
          amount = Option(200)
        )
      ),
      amount = 1000
    )))
    Ok(views.html.index("Invoice System"))
  }

  def createEvent = Action(parse.json) {
    rc =>
      println("received " + rc.toString())
      Ok(produceJS(ApiHandler.createEvent(rc.body.toString)))
  }

  def eventsByAuthor = Action(parse.json) {
    rc =>
      println("received " + rc.toString())
      Ok(produceJS(ApiHandler.eventsByAuthor(rc.body.toString)))
  }

  def eventsByParticipants = Action(parse.json) {
    rc =>
      println("received " + rc.toString())
      Ok(produceJS(ApiHandler.eventsByParticipants(rc.body.toString)))
  }

  def payment = Action(parse.json) {
    rc =>
      println("received " + rc.toString())
      Ok(produceJS(ApiHandler.makePayment(rc.body.toString)))
  }

  def events = Action {
    Ok(produceJS(ApiHandler.allEvents))
  }

  def eventsp = Action { rc =>
    Ok(produceJS(ApiHandler.allEvents))
  }

  def bothLists = Action(parse.json) { rc =>
    Ok(produceJS(ApiHandler.bothLists(rc.body.toString)))
  }

  def difference = Action(parse.json) {
    rc =>
      Ok(produceJS(ApiHandler.difference(rc.body.toString)))
  }

}