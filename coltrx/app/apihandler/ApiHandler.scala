package apihandler

import utils.JsonHelper
import transportModel._
import scala.util.{Failure, Success, Try}
import dao.BADao
import transportModel.TBoth
import transportModel.TPerson
import scala.util.Success
import scala.util.Failure
import transportModel.TEvent
import scala.Some
import transportModel.TPayment

/**
 * Created with IntelliJ IDEA.
 * User: hellraiser
 * Date: 7/26/14
 * Time: 7:55 PM
 * To change this template use File | Settings | File Templates.
 */
object ApiHandler extends JsonHelper {

  val ER = "ERROR"
  val OK = "OK"

  case class Response(
                  resultCode: String,
                  payload: List[Any] = List()
                       )

  def createEvent  = (ev: String) =>
    Try(extract[TEvent](ev)) match {
      case Success(event) => Try(BADao.save(event)) match {
        case Success(id) => Response(OK, List(event.copy(id = Some(id))))
        case Failure(err) => Response(ER, List("Couldn't save event: ERROR" + err.getMessage))
      }
      case Failure(err) => Response(ER, List(err.getMessage))
    }

  def eventsByAuthor = (ev: String) =>
    Try(extract[TPerson](ev)) match {
      case Success(person) => Try( BADao.lookupByAuthor(person) ) match {
        case Success(events) => Response(OK, List(events))
        case Failure(err) => Response(ER, List("Couldn't get events: ERROR" + err.getMessage))
      }
      case Failure(err) => Response(ER, List(err.getMessage))
    }

  def eventsByParticipants = (ev: String) =>
    Try(extract[TPerson](ev)) match {
      case Success(person) => Try(BADao.lookupByParticipant(person)) match {
        case Success(events) => Response(OK, List(events))
        case Failure(err) => Response(ER, List("Couldn't get events: ERROR" + err.getMessage))
      }
      case Failure(err) => Response(ER, List(err.getMessage))
    }

  def makePayment = (ev: String) =>
    Try(extract[TPayment](ev)) match {
      case Success(payment) => Try(BADao.pay(payment)) match {
        case Success(events) => Response(OK, List(events))
        case Failure(err) => Response(ER, List("Couldn't make payment: ERROR" + err.getMessage))
      }
      case Failure(err) => Response(ER, List(err.getMessage))
    }

  def allEvents = Try(BADao.allEvents) match {
      case Success(events) => Response(OK, List(events).flatten)
      case Failure(err) => Response(ER, List("Couldn't make payment: ERROR" + err.getMessage))
    }

  def bothLists = (ev: String) => Try(extract[TPerson](ev)) match {
      case Success(person) => {
        val owner = Try(BADao.lookupByAuthor(person)).toOption.toList.flatten
        val guest = Try(BADao.lookupByParticipant(person)).toOption.toList.flatten
        Response(OK, List(TBoth(owner, guest)))
      }
      case Failure(err) => Response(ER, List(err.getMessage))
    }
  
  def difference = (ev: String) =>
    Try(extract[TPerson](ev)) match {
      case Success(person) => {
        val owner = Try(BADao.lookupByAuthor(person)).toOption.toList.flatten
        val guest = Try(BADao.lookupByParticipant(person)).toOption.toList.flatten

        val loans = owner.flatMap(_.parts).groupBy(p => p.person) map { case (k, v) =>
          k -> v.foldRight(0.0)(  (loan, z) => z + loan.amount.getOrElse(0.0) - loan.payed.getOrElse(0.0))
        }
        val credits =  guest.map(e =>
          (e.author, e.parts.filter(_.person.map(_.number) == Option(person.number)))) map {
            case (k, v) =>
            k -> v.foldRight(0.0)( (credit, z ) => z + credit.payed.getOrElse(0.0) - credit.amount.getOrElse(0.0))
          }
        val grouped = (loans.toSeq ++ credits.toSeq).groupBy(_._1)
        val transform = grouped map { case (k, v) => k -> v.foldRight(0.0)( (diff, z) => z + diff._2 ) }
        val dseq = transform.toList.map(value => TDiff(value._1, value._2) )
        Response(OK, List(dseq).flatten)
      }
      case Failure(err) => Response(ER, List(err.getMessage))
    }


  /*def findDifference = (ev: String) =>
    Try(extract)*/

}
