package dao

import org.squeryl.Schema
import dbmodel.{InvoiceEvent, ParticipantSet, Person}

/**
 * Created with IntelliJ IDEA.
 * User: hellraiser
 * Date: 7/26/14
 * Time: 3:05 PM
 * To change this template use File | Settings | File Templates.
 */
object DBSchema extends Schema {

  val person = table[Person]("person")
  val participate = table[ParticipantSet]("participate")
  val invoice = table[InvoiceEvent]("invoice")

}
