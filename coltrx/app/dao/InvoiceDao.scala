package dao

import org.squeryl.PrimitiveTypeMode._
import dbmodel.{Person, InvoiceEvent}
import scala.util.Try

/**
 * Created with IntelliJ IDEA.
 * User: hellraiser
 * Date: 7/26/14
 * Time: 4:01 PM
 * To change this template use File | Settings | File Templates.
 */
object InvoiceDao extends SquerylSession {

  val inv = DBSchema.invoice

  def list = tx {
    inv.toList
  }

  def get = (id: String) => tx {
    inv.lookup(id)
  }

  def delete = (id: String) => tx {
    inv.delete(id)
  }

  def add = (i: InvoiceEvent) => Try {
    tx {
      inv.insert(i)
    }
    i.id
  }

  def findByAuthor = (p: Person) => tx {
    inv.where(i =>
      i.author === p.id
    ) toList
  }

  def findByParticipant = (p: Person) => tx {
    val st = DBSchema.participate.toList
    val st1 = st.filter(_.personid == p.id).map(_.eventid)
    inv.where(i =>
      i.id in st1
    ) toList
  }
}
