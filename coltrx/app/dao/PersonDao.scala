package dao

import org.squeryl.PrimitiveTypeMode._
import dbmodel.Person
import utils.IDGenerator
import transportModel.{TPerson, TPayment}

/**
 * Created with IntelliJ IDEA.
 * User: hellraiser
 * Date: 7/26/14
 * Time: 5:12 PM
 * To change this template use File | Settings | File Templates.
 */
object PersonDao extends SquerylSession with IDGenerator {

  val pt = DBSchema.person

  def list = tx {
    pt.toList
  }

  def get = (id: String) => tx {
    pt.lookup(id)
  }

  def add = (p: Person) => tx {
    pt.insert(p)
  }

  def findByNumber = (number: String) => tx {
    pt.where(p =>
      p.phone === number
    ) toList
  }

  def findFirstByNumber = (number: String) => findByNumber(number) headOption

  def append = (p: TPerson) => findFirstByNumber(p.number) match {
    case Some(p) => p
    case _ => pt.insert(Person(next.toString, p.number, p.fullname))
  }

}
