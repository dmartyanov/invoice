package dao

import utils.IDGenerator
import transportModel.{TParticipant, TPayment, TPerson, TEvent}
import dbmodel.{ParticipantSet, InvoiceEvent}
import java.sql.Timestamp
import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: hellraiser
 * Date: 7/26/14
 * Time: 5:50 PM
 * To change this template use File | Settings | File Templates.
 */
object BADao extends SquerylSession with IDGenerator {

  def save(ev: TEvent) = tx {
    val result = InvoiceDao.inv.insert(
      InvoiceEvent(
        id = next.toString,
        title = ev.title,
        place = "sber-hackaton",
        description = ev.description,
        amount = ev.amount,
        etype = 0,
        author = PersonDao.append(ev.author.get).id,
        date = Option(new Timestamp(new Date().getTime)),
        location = ev.location
      )
    )
    val definedAmount = ev.parts.filter(_.amount.isDefined).foldRight(0.0)( (item, z) => z + item.amount.getOrElse(0.0))
    val averageAmount = (ev.amount - definedAmount) / ev.parts.filter(_.amount.isEmpty).size
    ev.parts.foreach(part =>
      ParticipantSetDao.insert(
        ParticipantSet(
          eventid = result.id,
          personid = PersonDao.append(part.person.get).id,
          loanamount = averageAmount,
          payed = Some(0)
        )
      )
    )
    result.id
  }

  def lookupByAuthor = (p: TPerson) => (PersonDao.findByNumber(p.number) headOption match {
    case Some(person) => InvoiceDao.findByAuthor(person)
    case None => throw new IllegalArgumentException("Couldn't find person with number " + p.number)
  }) map extractTEvent

  def lookupByParticipant = (p: TPerson) => (PersonDao.findByNumber(p.number) headOption match {
    case Some(person) => InvoiceDao.findByParticipant(person)
    case None => throw new IllegalArgumentException("Couldn't find person with number " + p.number)
  }) map extractTEvent

  def allEvents = InvoiceDao.list map extractTEvent
  
  def pay = (op: TPayment) => PersonDao.findFirstByNumber(op.person.number) match {
    case Some(dbp) => ParticipantSetDao.get(op.eventid, dbp.id) match {
        case Some(ps) => ParticipantSetDao.changeAmount(ps.copy(
          payed = Some(ps.payed.getOrElse(0.0) + op.amount )
        ))
        case _ => throw new IllegalStateException("Couldn't detect person")
      }
    case _ => throw new IllegalStateException("Couldn't detect person")
  }

  def extractTEvent = (dbe: InvoiceEvent) => {
    val parts = ParticipantSetDao.findByEvent(dbe.id).map(extractPart)
    TEvent(
      author = extractPerson(dbe.author),
      title = dbe.title,
      description = dbe.description,
      amount = dbe.amount,
      etype = Option(dbe.etype),
      parts = parts,
      id = Option(dbe.id),
      date = dbe.date,
      location = dbe.location,
      payed = Option(parts.foldLeft(0.0)( (z, part) => z + part.payed.getOrElse(0.0)))
    )
  }

  def extractPart = (pset: ParticipantSet) => TParticipant(
      extractPerson(pset.personid),
      Option(pset.loanamount),
      pset.payed
    )

  def extractPerson = (pid: String) =>
    PersonDao.get(pid) collect { case p => TPerson(p.phone, p.fullname) }
}
