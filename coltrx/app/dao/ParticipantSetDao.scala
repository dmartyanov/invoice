package dao

import org.squeryl.PrimitiveTypeMode._
import dbmodel.ParticipantSet
import org.squeryl.dsl.CompositeKey2
import transportModel.TPayment

/**
 * Created with IntelliJ IDEA.
 * User: hellraiser
 * Date: 7/26/14
 * Time: 5:20 PM
 * To change this template use File | Settings | File Templates.
 */
object ParticipantSetDao extends SquerylSession {

  val pst = DBSchema.participate

  def list = tx {
    pst toList
  }

  def insert = (ps: ParticipantSet) => tx {
    pst.insert(ps)
  }

  def get(eventid: String, personid: String) = tx {
    pst.lookup(CompositeKey2(eventid, personid))
  }

  def findByEvent = (eventid: String) => tx {
    pst.where(ps => ps.eventid === eventid) toList
  }

  def findByPerson = (personid: String) => tx {
    pst.where(ps => ps.personid === personid) toList
  }

  def changeAmount = (ps: ParticipantSet) => tx { pst.update(ps) }
}
