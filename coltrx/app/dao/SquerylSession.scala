package dao

import org.squeryl.PrimitiveTypeMode._

/**
 * Created with IntelliJ IDEA.
 * User: hellraiser
 * Date: 7/26/14
 * Time: 4:09 PM
 * To change this template use File | Settings | File Templates.
 */
trait SquerylSession {

  def tx[P](l: => P) = inTransaction(l)
}