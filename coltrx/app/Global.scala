import org.squeryl.adapters.PostgreSqlAdapter
import org.squeryl.{Session, SessionFactory}
import play.api.db.DB
import play.api.{Application, GlobalSettings}

/**
 * Created with IntelliJ IDEA.
 * User: hellraiser
 * Date: 7/26/14
 * Time: 2:53 PM
 * To change this template use File | Settings | File Templates.
 */
object Global extends GlobalSettings {
  override def onStart(app: Application) {

    val dbAdapter = app.configuration.getString("db.default.driver") match {
      case Some("org.postgresql.Driver") => new PostgreSqlAdapter
      case _ => sys.error("Database driver must be org.postgresql.Driver")
    }
    SessionFactory.concreteFactory = Some(() => Session.create(DB.getConnection(autocommit = false)(app), dbAdapter))

  }
}
