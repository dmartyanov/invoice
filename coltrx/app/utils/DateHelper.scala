package utils

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

import play.api.Play._

/**
 * Created with IntelliJ IDEA.
 * User: dmartyanov
 * Date: 6/19/14
 * Time: 12:00 PM
 * To change this template use File | Settings | File Templates.
 *
 * default dateranges:
 * - 1 year
 * - 3 months
 */
trait DateHelper {

  val dateFormatter: SimpleDateFormat = new SimpleDateFormat("dd.MM.yyyy")
  lazy val months = configuration.getString("DateRange.periodInMonth").getOrElse("3").toInt
  lazy val years = configuration.getString("DateRange.periodInYears").getOrElse("1").toInt

  def calendar = Calendar.getInstance()

  def currentDate = calendar.getTime

  def currentDateStr = dateFormatter.format(currentDate)

  def dateTo = currentDate

  def dateToStr = dateFormatter.format(dateTo)

  def dateFromCalendar(unitAgo: Int, unitType: Int = Calendar.YEAR) = {
    val calendar = Calendar.getInstance()
    calendar.add(unitType, -unitAgo)
    calendar
  }

  def dateFromByYear(ys: Int = years) = dateFromCalendar(ys).getTime

  def dateFromByMonth(ms: Int = months) = dateFromCalendar(ms, Calendar.MONTH).getTime

  def dateFromByYearStr(ys: Int = years) = dateFormatter.format(dateFromByYear(ys))

  def dateFromByMonthStr(ms: Int = months) = dateFormatter.format(dateFromByMonth(ms))

  //hack
  def onlyDate = (dt: Date) => dateFormatter.parse(dateFormatter.format(dt))

}

object DateHelper extends DateHelper
