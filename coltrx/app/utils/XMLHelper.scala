package utils

import java.text.DateFormat
import java.util.Date

import org.json4s.Xml.toJson

import scala.xml.{Elem, Node}

/**
 * Created by d.a.martyanov on 29.05.14.
 */
trait XMLHelper {

  def parseInt(s: String) = try {Some(s.toInt)} catch {case _: Throwable => None}

  def parseDouble(s: String) = try {Some(s.toDouble)} catch {case _: Throwable => None}

  def parseDate(s: String, sdf: DateFormat) = try {Some(sdf parse s)} catch {case _: Throwable => None}

  def stringValue(node: Node, name: String) = for (v <- (node \ name).text) yield v

  def stringOption(node: Node, name: String) =
    stringValue(node, name) match {
      case str: String if str.isEmpty => None
      case str: String => Some(str)
    }

  def booleanValue(node: Node, name: String) = stringValue(node, name).trim.equals("true")

  def booleanOption(node: Node, name: String) = stringOption(node, name) collect {
    case value => value.trim.equals("true")
  }

  def doubleValue(node: Node, name: String) = parseDouble(stringValue(node, name)).getOrElse(0.0)

  def doubleOption(node: Node, name: String) = parseDouble(stringValue(node, name))

  def intOption(node: Node, name: String) = stringOption(node, name) flatMap {parseInt(_)}

  def dateValue(node: Node, name: String, sdf: DateFormat) = parseDate(stringValue(node, name), sdf).getOrElse(new Date)

  def dateOption(node: Node, name: String, sdf: DateFormat) = parseDate(stringValue(node, name), sdf)

  def xmlEquals(e1: Elem, e2: Elem) = toJson(e1) == toJson(e2)

  def xmlDiff(e1: Elem, e2: Elem) = toJson(e1) diff toJson(e2)

}

object XMLHelper extends XMLHelper
