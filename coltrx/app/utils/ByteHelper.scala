package utils

/**
 * Created by d.a.martyanov on 09.04.14.
 */
trait ByteHelper {

  def hex2bytes(hex: String): Array[Byte] =
    hex.replaceAll("[^0-9A-Fa-f]", "").sliding(2, 2).toArray.map(Integer.parseInt(_, 16).toByte)

  def bytes2hex(bytes: Array[Byte], separartor: Option[String] = None): String =
    bytes.map("%02x".format(_)).mkString(separartor.getOrElse(" "))
}

object ByteHelper extends ByteHelper

