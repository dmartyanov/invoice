package utils

/**
 * Created with IntelliJ IDEA.
 * User: dmartyanov
 * Date: 4/24/14
 * Time: 12:29 PM
 * To change this template use File | Settings | File Templates.
 */
trait FormHelper {

  protected def formStringParam(form: Map[String, Seq[String]], paramName: String) =
    form.get(paramName) collect {
      case Seq(value) if !value.isEmpty => value
    }
}

object FormHelper extends FormHelper
