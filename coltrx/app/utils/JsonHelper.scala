package utils

import org.json4s._
import org.json4s.jackson.Serialization.{write, writePretty}
import org.json4s.jackson.parseJson

import scala.util.Try

/**
 * Created by d.a.martyanov on 18.03.14.
 */

trait JsonHelper {

  implicit val formats: Formats = DefaultFormats

  def parseJS = (jsStr: String) => parseJson(jsStr)

  def produceJS = (obj: AnyRef) => write(obj)

  def pretty = (obj: AnyRef) => writePretty(obj)

  def jsChildValue(key: String, base: String) = (parseJS(base) \ key).toOption

  def childOpt[T: Manifest](key: String, base: String) = (jsChildValue(key, base) collect {
    case c => Try(c.extract[T]).toOption
  }).flatten

  def findJSValues(key: String, base: String) = parseJS(base) \\ key match {
    case jo: JObject => jo.children
    case jv: JValue => List(jv)
    case _ => List()
  }

  def findFirstJSValue(key: String, base: String) = findJSValues(key, base) headOption

  def find[T: Manifest](key: String, base: String) =
    for {
      jsValue <- findJSValues(key, base)
      tValue <- Try(jsValue.extract[T]).toOption.toList
    } yield tValue

  def findFirst[T: Manifest](key: String, base: String) = find[T](key, base) headOption

  def extract[T: Manifest](json: String) = parseJS(json).extract[T]
}

object JsonHelper extends JsonHelper
